m4_include(povray-helpers.m4)

# monster rules
povrayanim(plain-monster, 32, plain-monster.pov monster-blob.inc settings.inc)
povrayrandom(plain-gore, 8, plain-gore.pov gore-blob.inc settings.inc)

povrayanim(hard-monster, 48, hard-monster.pov monster-blob.inc settings.inc)
povrayrandom(hard-gore, 8, hard-gore.pov gore-blob.inc settings.inc)

povrayanim(seeker-monster, 32, seeker-monster.pov monster-blob.inc settings.inc)
povrayrandom(seeker-gore, 8, seeker-gore.pov gore-blob.inc settings.inc)

povrayanim(egg-layer-monster, 48, egg-layer-monster.pov monster-blob.inc settings.inc)
povrayrandom(egg-layer-gore, 8, egg-layer-gore.pov gore-blob.inc settings.inc)

# egg rules
povrayanim(plain-egg, 4, plain-egg.pov egg.inc settings.inc, Declare=sval=1)
povrayanim(hard-egg, 4, hard-egg.pov egg.inc settings.inc, Declare=sval=2)
povrayanim(seeker-egg, 4, seeker-egg.pov egg.inc settings.inc, Declare=sval=3)
povrayanim(egg-layer-egg, 4, egg-layer-egg.pov egg.inc settings.inc, Declare=sval=4)

# hero
povraytile(hero-right, hero.pov hero.inc settings.inc, Declare=hero_angle=0)
povraytile(hero-right-down, hero.pov hero.inc settings.inc, Declare=hero_angle=45)
povraytile(hero-down, hero.pov hero.inc settings.inc, Declare=hero_angle=90)
povraytile(hero-left-down, hero.pov hero.inc settings.inc, Declare=hero_angle=135)
povraytile(hero-left, hero.pov hero.inc settings.inc, Declare=hero_angle=180)
povraytile(hero-left-up, hero.pov hero.inc settings.inc, Declare=hero_angle=225)
povraytile(hero-up, hero.pov hero.inc settings.inc, Declare=hero_angle=270)
povraytile(hero-right-up, hero.pov hero.inc settings.inc, Declare=hero_angle=315)

povraytile(invisible-hero-right, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=0)
povraytile(invisible-hero-right-down, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=45)
povraytile(invisible-hero-down, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=90)
povraytile(invisible-hero-left-down, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=135)
povraytile(invisible-hero-left, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=180)
povraytile(invisible-hero-left-up, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=225)
povraytile(invisible-hero-up, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=270)
povraytile(invisible-hero-right-up, invisible-hero.pov hero.inc settings.inc, Declare=hero_angle=315)

povrayrandom(hero-gore, 8, hero-gore.pov gore-blob.inc settings.inc)

# misc
povraytile(block, block.pov settings.inc)
povraytile(wall, wall.pov settings.inc)
povraytile(monster-wall, monster-wall.pov settings.inc)
povraytile(freeze-box, freeze-box.pov settings.inc)
povraytile(power-up-invisibility, power-up-invisibility.pov power-up.inc settings.inc)
povraytile(power-up-freeze, power-up-freeze.pov power-up.inc settings.inc)
povraytile(power-up-explode, power-up-explode.pov power-up.inc settings.inc)
povrayrandom(levitation, 8, levitation.pov settings.inc)
