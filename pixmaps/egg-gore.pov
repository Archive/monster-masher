#include "colors.inc"
#include "stones.inc"

#include "settings.inc"

#declare rs = seed(sval);
#declare rs2 = seed(sval);

// from shapes2.inc
#declare Dodecahedron = 
 intersection 
  {plane {-z, 1 rotate <-26.56505117708,    0, 0>}
   plane {-z, 1 rotate <-26.56505117708,  -72, 0>}
   plane {-z, 1 rotate <-26.56505117708, -144, 0>}
   plane {-z, 1 rotate <-26.56505117708, -216, 0>}
   plane {-z, 1 rotate <-26.56505117708, -288, 0>}
   
   plane {-z, 1 rotate <26.56505117708,  -36, 0>}
   plane {-z, 1 rotate <26.56505117708, -108, 0>}
   plane {-z, 1 rotate <26.56505117708, -180, 0>}
   plane {-z, 1 rotate <26.56505117708, -252, 0>}
   plane {-z, 1 rotate <26.56505117708, -324, 0>}
   
   plane { y, 1}
   plane {-y, 1}
   
   bounded_by {sphere {0, 1.2585}}
  }

#declare egg =
union {
   difference {
      sphere { 0, 1 }
      plane { y, 0 }
      scale <1.1, 1.7, 1.1>
   }

   difference {
      sphere { 0, 1 }
      plane { -y, 0 }
      scale <1.1, 1.3, 1.1>
   }
}

union {
#declare i = 0;
#while (i < 10)
intersection {
   difference {
      object { egg }
      object { egg scale 0.95 }
   }
   #declare pos = from_spherical(1, pi * rand(rs), 2 * pi * rand(rs));
   object {
      Dodecahedron
      scale <0.4 + 0.3 * rand(rs), 0.4 + 0.3 * rand(rs), 0.4 + 0.3 * rand(rs)>
      translate pos
   }

   texture { T_Stone29 }

   //translate -pos
   scale 35
   rotate <360 * rand(rs), 360 * rand(rs), 360 * rand(rs)>

   #declare ang = 2 * pi * rand(rs2);
   #declare rad = 15 + 30 * rand(rs2);
   //translate <rad * cos(ang), rad * sin(rad), 0>
}
#declare i = i + 1;
#end
}
