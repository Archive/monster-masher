m4_dnl handsome helper from the info pages for m4
m4_define(`forloop',`m4_pushdef(`$1', `$2')_forloop(`$1', `$2', `$3', `$4')m4_popdef(`$1')')m4_dnl
m4_define(`_forloop',`$4`'m4_ifelse($1, `$3', ,`m4_define(`$1', m4_incr($1))_forloop(`$1', `$2', `$3', `$4')')')m4_dnl
m4_dnl
m4_dnl povraytilehelper(basename, dependencies, size, extra flags)
m4_define(povraytilehelper, m4_dnl
`$1-$3.png: $2
	$(POVRAY) $(POVFLAGS) +o$`'@ +w$3 +h$3 $< $4

generated_images += $1-$3.png
')
m4_dnl
m4_dnl povraytile(basename, dependencies, extra flags)
m4_define(povraytile, m4_dnl
`povraytilehelper(`$1', `$2', 32, $3)
povraytilehelper(`$1', `$2', 24, $3)')
m4_dnl
m4_dnl povrayanimhelper(basename, no. of images, dependencies, size, extra flags)
m4_define(povrayanimhelper, m4_dnl
`m4_pushdef(`imagei', `$1-$4-m4_format(%0`'m4_len($2)d, i).png')
forloop(`i', 1, $2,
`imagei: $3
	$(POVRAY) $(POVFLAGS) +o$1-$4-.png +kfi1 +kff$2 +sf`'i +ef`'i +w$4 +h$4 $< $5
')
$1-$4.png: forloop(`i', 1, $2, `imagei ')
	convert +append $+ $`'@

generated_images += $1-$4.png
temporary_images += forloop(`i', 1, $2, `imagei ')
m4_popdef(`imagei')')
m4_dnl
m4_dnl povrayanim(basename, no. of images, dependencies, extra flags)
m4_define(povrayanim, m4_dnl
`povrayanimhelper(`$1', `$2', `$3', 32, $4)
povrayanimhelper(`$1', `$2', `$3', 24, $4)')
m4_dnl
m4_dnl povrayrandomhelper(basename, no. of images, dependencies, size, extra flags)
m4_define(povrayrandomhelper, m4_dnl
`m4_pushdef(`imagei', `$1-$4-m4_format(%0`'m4_len($2)d, i).png')
forloop(`i', 1, $2,
`imagei: $3
	$(POVRAY) $(POVFLAGS) +o$1-$4-i.png +w$4 +h$4 $< Declare=sval=i $5
')
$1-$4.png: forloop(`i', 1, $2, `imagei ')
	convert +append $+ $`'@

generated_images += $1-$4.png
temporary_images += forloop(`i', 1, $2, `imagei ')
m4_popdef(`imagei')')
m4_dnl povrayrandom(basename, no. of images, dependencies, extra flags)
m4_define(povrayrandom, m4_dnl
`povrayrandomhelper(`$1', `$2', `$3', 32, $4)
povrayrandomhelper(`$1', `$2', `$3', 24, $4)')
